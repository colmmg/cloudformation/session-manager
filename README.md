# cloudformation/session-manager
This CloudFormation template creates the neccessary resources to use [AWS Systems Manager - Session Manager](https://docs.aws.amazon.com/systems-manager/latest/userguide/session-manager.html) for connecting to EC2 instances.

# License and Authors
Authors: [Colm McGuigan](https://gitlab.com/colmmg)
